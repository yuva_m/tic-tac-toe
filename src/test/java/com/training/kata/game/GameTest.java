package com.training.kata.game;

import com.training.kata.game.exceptions.PositionNotAvailableException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class GameTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private Game game;

    @Before
    public void init() {
        game = new Game();
    }

    @Test
    public void gameShouldAlwaysStartWithPlayerX() throws PositionNotAvailableException {
        game.play(0, 1);

        assertThat(game.getNextPlayer(), is("O"));
    }

    @Test
    public void getNextPlayerShouldSwitchPlayerAfterEachMark() throws PositionNotAvailableException {
        game.play(0, 1);
        game.play(0, 2);

        assertThat(game.getNextPlayer(), is("X"));
    }

    @Test
    public void playShouldThrowPositionNotAvailableExceptionWhenGivenPositionIsAlreadyMarked() throws PositionNotAvailableException {
        expectedException.expect(PositionNotAvailableException.class);
        expectedException.expectMessage("Position {0, 1} is already marked");

        game.play(0, 1);
        game.play(0, 1);
    }

    @Test
    public void playShouldReturnMessageXHasWonWhenPlayerXMarksEntireFirstRow() throws PositionNotAvailableException {
        game.play(0, 0);
        game.play(1, 0);
        game.play(0, 1);
        game.play(1, 2);
        String result = game.play(0, 2);

        assertThat(result, is("Player X Won!"));
    }

    @Test
    public void playShouldReturnMessageOHasWonWhenPlayerOMarksEntireFirstRow() throws PositionNotAvailableException {
        game.play(1, 0);
        game.play(0, 0);
        game.play(1, 2);
        game.play(0, 1);
        game.play(2, 1);
        String result = game.play(0, 2);

        assertThat(result, is("Player O Won!"));
    }

    @Test
    public void playShouldReturnMessageContinueWhenEntireFirstRowIsNotMarkedByAnyPlayer() throws PositionNotAvailableException {
        game.play(1, 0);
        game.play(0, 0);
        game.play(1, 2);
        game.play(2, 1);
        game.play(2, 2);
        String result = game.play(0, 2);

        assertThat(result, is("Continue!"));
    }

    @Test
    public void playShouldReturnMessageXHasWonWhenPlayerXMarksEntireSecondRow() throws PositionNotAvailableException {
        game.play(1, 0);
        game.play(0, 0);
        game.play(1, 1);
        game.play(0, 2);
        String result = game.play(1, 2);

        assertThat(result, is("Player X Won!"));
    }

    @Test
    public void playShouldReturnMessageXHasWonWhenPlayerXMarksEntireThirdRow() throws PositionNotAvailableException {
        game.play(2, 0);
        game.play(0, 0);
        game.play(2, 1);
        game.play(0, 2);
        String result = game.play(2, 2);

        assertThat(result, is("Player X Won!"));
    }

    @Test
    public void playShouldReturnMessageOHasWonWhenPlayerXMarksEntireThirdRow() throws PositionNotAvailableException {
        game.play(0, 0);
        game.play(2, 0);
        game.play(0, 2);
        game.play(2, 1);
        game.play(1, 1);
        String result = game.play(2, 2);

        assertThat(result, is("Player O Won!"));
    }
}
