package com.training.kata.game;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class BoardTest {

    private Board board;

    public BoardTest() {
    }

    @Before
    public void init() {
        board = new Board();
    }

    @Test
    public void playerXMarkShouldBeCapturedWhenPlayedAtRow0Column0() {
        board.play("X", 0, 0);

        assertThat(board.getPlayerAt(0, 0), Is.is("X"));
    }

    @Test
    public void isPositionAvailableShouldReturnFalseWhenACellIsAlreadyMarked() {
        board.play("X", 0, 1);

        assertFalse(board.isPositionAvailable(0, 1));
    }

    @Test
    public void isPositionAvailableShouldReturnTrueWhenACellIsNotMarked() {
        assertTrue(board.isPositionAvailable(0, 1));
    }

    @Test
    public void getLastPlayedPositionShouldReturnLastPlayedPosition() {
        board.play("X", 0, 1);

        assertTrue(Arrays.equals(board.getLastPlayedPosition(), new int[]{0, 1}));
    }

    @Test
    public void getLastPlayedPositionShouldReturnEmptyArrayWhenGameIsStarted() {
        assertTrue(Arrays.equals(board.getLastPlayedPosition(), new int[2]));
    }

    @Test
    public void getReturnNumberOfSuccessfulPlaysShouldReturn3After3SuccessfulMarks() {
        board.play("X", 0, 0);
        board.play("X", 0, 1);
        board.play("X", 0, 2);

        assertThat(board.getNumberOfSuccessfulPlays(), Is.is(3));
    }

    @Test
    public void getReturnNumberOfSuccessfulPlaysShouldReturn4After4SuccessfulMarks() {
        board.play("X", 0, 0);
        board.play("X", 0, 1);
        board.play("X", 0, 2);
        board.play("X", 1, 2);

        assertThat(board.getNumberOfSuccessfulPlays(), Is.is(4));
    }
}