package com.training.kata.game;

class Board {
    private final String[][] cells;
    private int[] lastPlayedPosition;
    private int numberOfSuccessfulPlays;

    Board() {
        cells = new String[3][3];
        lastPlayedPosition = new int[2];
        numberOfSuccessfulPlays = 0;
    }

    void play(String playerName, int rowPosition, int columnPosition) {
        cells[rowPosition][columnPosition] = playerName;
        lastPlayedPosition[0] = rowPosition;
        lastPlayedPosition[1] = columnPosition;
        numberOfSuccessfulPlays++;
    }

    String getPlayerAt(int rowPosition, int columnPosition) {
        return cells[rowPosition][columnPosition];
    }

    boolean isPositionAvailable(int rowPosition, int columnPosition) {
        return cells[rowPosition][columnPosition] == null;
    }

    int[] getLastPlayedPosition() {
        return lastPlayedPosition;
    }

    int getNumberOfSuccessfulPlays() {
        return numberOfSuccessfulPlays;
    }
}