package com.training.kata.game.exceptions;

public class PositionNotAvailableException extends Exception {
    public PositionNotAvailableException(int row, int col) {
        super("Position {"+row+", "+col+"} is already marked");
    }
}
