package com.training.kata.game;

import com.training.kata.game.exceptions.PositionNotAvailableException;

class Game {

    private Board board = new Board();

    String getNextPlayer() {
        int[] lastPlayedPosition = board.getLastPlayedPosition();

        String lastPlayer = board.getPlayerAt(lastPlayedPosition[0], lastPlayedPosition[1]);

        return "X".equals(lastPlayer) ? "O" : "X";
    }

    String play(int row, int col) throws PositionNotAvailableException {
        if(board.isPositionAvailable(row, col)) {
            board.play(getNextPlayer(), row, col);
            return getGameResult();
        }

        throw new PositionNotAvailableException(row, col);
    }

    private String getGameResult() {
        if(isXWon()) {
            return "Player X Won!";
        } else if(isOWon()) {
            return "Player O Won!";
        } else {
            return "Continue!";
        }
    }

    private boolean isOWon() {
        return isAnyRowCompletelyMarkedByPlayer("O");
    }

    private boolean isXWon() {
        return isAnyRowCompletelyMarkedByPlayer("X");
    }

    private boolean isAnyRowCompletelyMarkedByPlayer(String player) {
        return isFullRowMarkedByPlayer(0, player) ||
                isFullRowMarkedByPlayer(1, player) ||
                isFullRowMarkedByPlayer(2, player);
    }

    private boolean isFullRowMarkedByPlayer(int row, String player) {
        String playerAtCell1 = board.getPlayerAt(row, 0);
        String playerAtCell2 = board.getPlayerAt(row, 1);
        String playerAtCell3 = board.getPlayerAt(row, 2);

        return player.equals(playerAtCell1) && player.equals(playerAtCell2) && player.equals(playerAtCell3);
    }
}
